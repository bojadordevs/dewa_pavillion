﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppManagerArtNet : MonoBehaviour
{
    const string CONFIG_FILE = "Conf.ini";

    [SerializeField] GameObject m_ledConfig;
    [SerializeField] UDPSend m_udpSendClient;
    [SerializeField] int portSender = 8002;
    [SerializeField] int portReceiver = 8001;


    void Start()
    {
        Invoke("EnableLEDConfig", 1);
    }

    void EnableLEDConfig()
    {
        ArtnetController.instance.ledDoneAnim += DoneLedAnimation;
        m_ledConfig.SetActive(false);
    }

    public void DoneLedAnimation()
    {
        Debug.Log("DoneLedAnimation");
        m_udpSendClient.SendUDPMsg("DLA"); //done led animation
        //SendOSC();
    }


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            m_ledConfig.SetActive(!m_ledConfig.activeInHierarchy);
        }
    }

    public void DisableLedConfig()
    {
        m_ledConfig.SetActive(false);
    }

    public void SetChannel(string p_commandName)
    {
        string[] dmxConfig = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/DMXData/" + p_commandName + ".ini");
        int universe = 0;
        List<Channel> channelList = new List<Channel>();
        for (int i = 0; i < dmxConfig.Length; i++)
        {
            if (i == 0)
            {
                universe = int.Parse(dmxConfig[i].Split('=')[1]);
            }
            else
            {
                Channel channel = new Channel();
                channel.index = int.Parse(dmxConfig[i].Split(',')[0].Split('=')[1]);
                channel.val = System.Convert.ToByte(dmxConfig[i].Split(',')[1].Split('=')[1]);
                channelList.Add(channel);
            }
        }
        ArtnetController.instance.SetChannelByUniverse(universe, channelList);
    }

    IEnumerator ReloadScene()
    {
        //m_loadingScreen.SetActive(true);
        ArtnetController.instance.TurnOffAllLED();
        AsyncOperation asyncLoad = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(0);
        while (!asyncLoad.isDone)
            yield return null;
        //m_loadingScreen.SetActive(false);
    }
}
