﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LiteShared.Dmx_In;
//using Free_Float_Player_dotnet;
using System;
using UnityEngine.UI;

public class ArtnetController : MonoBehaviour
{
    const int MAX_PIXEL_PER_UNIVERSE = 128;
    const int MAX_DMX_LENGTH = 512;

    static ArtNet.Engine ArtEngine = new ArtNet.Engine("Artnet controller", "");

    public static ArtnetController instance;
    public delegate void OnLedDoneAnim();
    public event OnLedDoneAnim ledDoneAnim;
    public InputField m_refUniverse;

    [SerializeField] int m_maxUniverse = 10;
    [SerializeField] bool m_enableArtnet = false;
    [SerializeField] GameObject m_artnetWarning;
    [SerializeField] UniverseObject[] universeList;

    //Added for custom channel
    byte[] m_DMXData; // for storing state

    void Awake()
    {
        instance = this;
        Application.targetFrameRate = 300;

        //Initialize Universe List 
        for (short i = 0; i < universeList.Length; i++)
            universeList[i].universeId = i;

        m_refUniverse.text = PlayerPrefs.GetString("REFUNIVERSE", "0");
    }

    void Start()
    {

#if !UNITY_EDITOR
    m_enableArtnet = true;
#endif
        if (m_enableArtnet)
        {
            ArtEngine.Start();
            TurnOffAllLED();
        }
        try
        {
            m_artnetWarning.gameObject.SetActive(!m_enableArtnet);
        }
        catch
        {
            Debug.Log("-----------No artnet warning!!!!--------------");
        }
        m_DMXData = new byte[MAX_DMX_LENGTH];
    }
    public void PlayAnimation()
    {
        PlayLedAnimation();
        //Debug.Log("PlayLedAnimation");
    }

    public void PlayLedAnimation(Pixel p_pixel = null)
    {
        TurnOffAllLED();
        for (int i = 0; i < universeList.Length; i++)
        {
            if (!universeList[i].GetLedConfig().isExtension && universeList[i].GetLedConfig().isEnable)
            {
                Debug.Log(i + " : StartLedAnimation");
                StartCoroutine(StartLedAnimation(universeList[i].GetLedConfig(), 0, p_pixel));
            }
        }
    }

    public void PlayLedAnimationByUniverse(int p_universe, Pixel p_pixel = null)
    {
        TurnOffLEDByUniverse(p_universe);
        if (!universeList[p_universe].GetLedConfig().isExtension && universeList[p_universe].GetLedConfig().isEnable)
            StartCoroutine(StartLedAnimation(universeList[p_universe].GetLedConfig(), 0, p_pixel));
    }

    public void CloseAllColorPicker()
    {
        for (int i = 0; i < universeList.Length; i++)
        {
            universeList[i].CloseColorPicker();
        }
    }

    public IEnumerator StartLedAnimation(LEDConfig p_ledConfig, int p_totalPixel, Pixel p_pixel = null)
    {
        yield return null;
        byte[] _DMXData = new byte[MAX_DMX_LENGTH];
        int pixelCtr = 0;
        bool isPlaying = false;
        int startPixel = p_totalPixel;

        for (int sidePixelCnt1 = 0; sidePixelCnt1 < MAX_PIXEL_PER_UNIVERSE + 1; sidePixelCnt1++)
        {

            if (p_pixel == null)
                SendToArtnet(p_ledConfig.pixelColor, p_ledConfig, p_ledConfig.universe, ref pixelCtr);
            else
                SendToArtnet(p_pixel, p_ledConfig, p_ledConfig.universe, ref pixelCtr);


            if (p_totalPixel >= p_ledConfig.startPxl)
            {
                Debug.Log(startPixel + " : " + sidePixelCnt1);
                yield return new WaitForSeconds(p_ledConfig.delay);
            }

            Debug.Log((ledDoneAnim != null) + " : " + p_totalPixel + " : " + p_ledConfig.endPxl + " : " + !isPlaying);
            if (ledDoneAnim != null && p_totalPixel >= p_ledConfig.endPxl && !isPlaying)
            {
                //Debug.Log(m_refUniverse.text + " : " + p_ledConfig.universe);
                if (short.Parse(m_refUniverse.text) == p_ledConfig.universe)
                {
                    ledDoneAnim();
                    isPlaying = true;
                    Debug.Log("------------DONE ANIMATION----------- universe" + p_ledConfig.universe);
                }
                break;
            }
            else
            {
                p_totalPixel++;
            }
        }

        if (p_ledConfig.nextUniverse > 0 && universeList[p_ledConfig.nextUniverse].GetLedConfig().isEnable && universeList[p_ledConfig.nextUniverse].GetLedConfig().isExtension)
        {
            p_totalPixel--;
            StartCoroutine(StartLedAnimation(universeList[p_ledConfig.nextUniverse].GetLedConfig(), p_totalPixel));
        }
    }

    void SendToArtnet(Pixel p_pixel, LEDConfig p_ledConfig, short p_universe, ref int p_pixelCtr)
    {
        byte[] _DMXData = new byte[MAX_DMX_LENGTH];

        if (!p_ledConfig.isInvert)
        {
            //Debug.Log("Normal");
            for (int i = 0; i < _DMXData.Length - 1; i++)
            {
                if (i < p_pixelCtr)
                {
                    _DMXData[i++] = p_pixel.red;
                    _DMXData[i++] = p_pixel.green;
                    _DMXData[i++] = p_pixel.blue;
                    _DMXData[i] = p_pixel.white;
                }
            }
        }
        else
        {
            //Debug.Log("Inverted");
            for (int i = _DMXData.Length - 1; i > 0; i--)
            {
                if (i > _DMXData.Length - p_pixelCtr)
                {
                    _DMXData[i--] = p_pixel.white;
                    _DMXData[i--] = p_pixel.blue;
                    _DMXData[i--] = p_pixel.green;
                    _DMXData[i] = p_pixel.red;
                }
            }
        }

        p_pixelCtr += 4;

        if (m_enableArtnet)
            ArtEngine.SendDMX(p_universe, _DMXData, _DMXData.Length);
    }

    public void TurnOffAllLED()
    {
        StopAllCoroutines();
        //Debug.Log("TurnOffAllLED!");
        byte[] _DMXData = new byte[MAX_DMX_LENGTH];
        for (int i = 0; i < MAX_DMX_LENGTH; i++)
        {
            _DMXData[i] = Convert.ToByte(0);
        }
        if (m_enableArtnet)
        {
            for (int i = 0; i < m_maxUniverse; i++)
                ArtEngine.SendDMX((short)i, _DMXData, _DMXData.Length);
        }
    }

    //------Added for custom channels

    public void SetChannelByUniverse(int p_universe , List<Channel> p_channelList)
    {
        //Debug.Log("SetChannelByUniverse! " + p_universe);

        for (int i = 0; i < p_channelList.Count; i++)
        {
            m_DMXData[p_channelList[i].index] = p_channelList[i].val;
            Debug.Log(p_channelList[i].index + " "+ m_DMXData[p_channelList[i].index]);
        }

        if (m_enableArtnet)
            ArtEngine.SendDMX((short)p_universe, m_DMXData, m_DMXData.Length);
    }

    //------Added for custom channels

    public void TurnOffLEDByUniverse(int p_universe)
    {
        //Debug.Log("TurnOffAllLED!");
        byte[] _DMXData = new byte[MAX_DMX_LENGTH];
        for (int i = 0; i < MAX_DMX_LENGTH; i++)
        {
            _DMXData[i] = Convert.ToByte(0);
        }
        if (m_enableArtnet)
            ArtEngine.SendDMX((short)p_universe, _DMXData, _DMXData.Length);
    }

    public void TurnOnAllLED( )
    {
        StopAllCoroutines();
        byte[] _DMXData = new byte[MAX_DMX_LENGTH];

        //Debug.Log("---Turn on all LED----");
        for (int i = 0; i <= _DMXData.Length - 1; i++)
        {
            _DMXData[i] = Convert.ToByte(255);
        }
        if (m_enableArtnet)
        {
            for (int i = 0; i < m_maxUniverse; i++)
                ArtEngine.SendDMX((short)i, _DMXData, _DMXData.Length);
        }
    }

    public void TurnOnAllLED(Pixel p_pixel)
    {
        StopAllCoroutines();
        byte[] _DMXData = new byte[MAX_DMX_LENGTH];

        //Debug.Log("---Turn on all LED----");
        for (int i = 0; i < _DMXData.Length - 1; i++)
        {
            _DMXData[i++] = p_pixel.red;
            _DMXData[i++] = p_pixel.green;
            _DMXData[i++] = p_pixel.blue;
            _DMXData[i] = p_pixel.white;
        }

        if (m_enableArtnet)
        {
            for (int i = 0; i < m_maxUniverse; i++)
                ArtEngine.SendDMX((short)i, _DMXData, _DMXData.Length);
        }
    }

    public void TurnOnLEDByUniverse(Pixel p_pixel,int p_universe)
    {
        StopAllCoroutines();
        byte[] _DMXData = new byte[MAX_DMX_LENGTH];

        //Debug.Log("---Turn on all LED----");
        for (int i = 0; i < _DMXData.Length - 1; i++)
        {
            _DMXData[i++] = p_pixel.red;
            _DMXData[i++] = p_pixel.green;
            _DMXData[i++] = p_pixel.blue;
            _DMXData[i] = p_pixel.white;
        }
        if (m_enableArtnet)
            ArtEngine.SendDMX((short)p_universe, _DMXData, _DMXData.Length);
    }

    public void SaveAll()
    {
        for (short i = 0; i < universeList.Length; i++)
        {
            universeList[i].SaveConfig();
        }
        PlayerPrefs.SetString("REFUNIVERSE", m_refUniverse.text);
    }

    public void ClearPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

    private void OnDestroy()
    {
        SaveAll();
        if (m_enableArtnet)
            ArtEngine.Pause();

    }

    void OnApplicationQuit()
    {
#if !UNITY_EDITOR
        ArtEngine.Pause();
        // we need this line to insure the build will close disable this during development or else unity will close each time you exit play mode 
        System.Diagnostics.Process.GetCurrentProcess().Kill();
#endif
    }
}

[Serializable]
public class Pixel
{
    public Byte red = Convert.ToByte(0); //Red;
    public Byte green = Convert.ToByte(0); //blue;
    public Byte blue = Convert.ToByte(0); //green;
    public Byte white = Convert.ToByte(0); //white;
}

[Serializable]
public class LEDConfig
{
    public bool isEnable;
    public short universe;
    public float delay;
    public int startPxl;
    public int endPxl;
    public bool isInvert;
    public bool isExtension;
    public Pixel pixelColor;
    public int nextUniverse;
}

[Serializable]
public class Channel
{
    public int index = 0;
    public Byte val = Convert.ToByte(0);
}
