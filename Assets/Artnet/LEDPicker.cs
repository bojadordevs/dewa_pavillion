﻿using System.Globalization;
using UnityEngine;

namespace UnityEngine.UI.Extensions.ColorPicker
{
    [RequireComponent(typeof(Image))]
    public class LEDPicker : MonoBehaviour
    {
        int universe = 0;
        public ColorPickerControl picker;

        Image image;

        private void Awake()
        {
            //PlayerPrefs.DeleteAll();
            image = GetComponent<Image>();
            picker.onValueChanged.AddListener(ColorChanged);
        }

        private void OnDestroy()
        {
            picker.onValueChanged.RemoveListener(ColorChanged);
        }


        private void ColorChanged(Color newColor)
        {
            image.color = newColor;
            //Debug.Log("picker: " + picker.R + "," + picker.G + "," + picker.B + "," + picker.V);
        }

        public void InitUniverse(int p_universeId) {
            universe = p_universeId;
            //EnablePicker();
            LoadColor();
        }

        public void EnablePicker()
        {
            picker.gameObject.SetActive(!picker.gameObject.activeInHierarchy);
        }

        public void SaveColor()
        {
            PlayerPrefs.SetFloat("LEDColorR" + universe,picker.R);
            PlayerPrefs.SetFloat("LEDColorG" + universe,picker.G);
            PlayerPrefs.SetFloat("LEDColorB" + universe,picker.B);
            PlayerPrefs.SetFloat("LEDColorV" + universe,picker.V);
            //Debug.Log("SAVE COLOR" + picker.R + " : " + picker.G + " : " + picker.B + " : " + picker.V);
        }

        public void LoadColor()
        {
            picker.R = PlayerPrefs.GetFloat("LEDColorR" + universe, 255);
            picker.G = PlayerPrefs.GetFloat("LEDColorG" + universe, 255);
            picker.B = PlayerPrefs.GetFloat("LEDColorB" + universe, 255);
            picker.V = PlayerPrefs.GetFloat("LEDColorV" + universe, 0);
            //Debug.Log("LOAD COLOR");

        }

        public Pixel GetPixelColor()
        {
            Pixel pixelColor = new Pixel();
            pixelColor.red = System.Convert.ToByte(picker.R * 255);
            pixelColor.green = System.Convert.ToByte(picker.G * 255);
            pixelColor.blue = System.Convert.ToByte(picker.B * 255);
            pixelColor.white = 200;
            //pixelColor.white = System.Convert.ToByte(picker.CurrentColor.a * 255);
            return pixelColor;
        }
    }
}