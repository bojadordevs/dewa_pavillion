﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions.ColorPicker;

public class UniverseObject : MonoBehaviour
{
    public LEDConfig ledConfig;
    public short universeId = 0;

    [SerializeField] Toggle m_enable;
    [SerializeField] Text m_universeTxt;
    [SerializeField] InputField m_startPixel;
    [SerializeField] InputField m_endPixel;
    [SerializeField] InputField m_delayTime;
    [SerializeField] Toggle m_isInvert;
    [SerializeField] Toggle m_isExtension;
    [SerializeField] InputField m_nextUniverse;
    [SerializeField] LEDPicker m_ledPicker;

    public UniverseObject(short p_universeID)
    {
        universeId = p_universeID;
    }

    public void CloseColorPicker()
    {
        m_ledPicker.picker.gameObject.SetActive(false);
    }

    void Start()
    {
        m_universeTxt.text = "Universe " + universeId;
        m_ledPicker.InitUniverse(universeId);
        LoadConfig();
    }

    private void OnDisable()
    {
        SaveConfig();
    }

    public void SaveConfig()
    {
        PlayerPrefs.SetInt("UO_enable" + universeId, m_enable.isOn ? 1 : 0);
        PlayerPrefs.SetString("UO_nextUniv" + universeId, m_nextUniverse.text);
        PlayerPrefs.SetString("UO_SP" + universeId, m_startPixel.text);
        PlayerPrefs.SetString("UO_EP" + universeId, m_endPixel.text);
        PlayerPrefs.SetString("UO_Delay" + universeId, m_delayTime.text);
        PlayerPrefs.SetInt("UO_inv" + universeId, m_isInvert.isOn ? 1 : 0);
        PlayerPrefs.SetInt("UO_ext" + universeId, m_isExtension.isOn ? 1 : 0);
        m_ledPicker.SaveColor();
    }


    public void LoadConfig()
    {
        ledConfig.universe = universeId;
        m_enable.isOn = PlayerPrefs.GetInt("UO_enable" + universeId, 0) == 1;
        m_nextUniverse.text = PlayerPrefs.GetString("UO_nextUniv" + universeId, "0");
        m_startPixel.text = PlayerPrefs.GetString("UO_SP" + universeId, "0");
        m_endPixel.text = PlayerPrefs.GetString("UO_EP" + universeId, "128");
        m_delayTime.text = PlayerPrefs.GetString("UO_Delay" + universeId, "0.1");
        m_isInvert.isOn = PlayerPrefs.GetInt("UO_inv" + universeId, 0) == 1;
        m_isExtension.isOn = PlayerPrefs.GetInt("UO_ext" + universeId, 0) == 1;
    }

    public LEDConfig GetLedConfig()
    {
        LEDConfig ledConfig = new LEDConfig();
        ledConfig.universe = universeId;
        ledConfig.isEnable = m_enable.isOn;
        ledConfig.startPxl = int.Parse(m_startPixel.text);
        ledConfig.endPxl = int.Parse(m_endPixel.text);
        ledConfig.delay = float.Parse(m_delayTime.text);
        ledConfig.pixelColor = m_ledPicker.GetPixelColor();
        ledConfig.nextUniverse = int.Parse(m_nextUniverse.text);
        ledConfig.isInvert = m_isInvert.isOn;
        ledConfig.isExtension = m_isExtension.isOn;
        return ledConfig;
    }
}
