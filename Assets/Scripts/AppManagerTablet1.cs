using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppManagerTablet1 : MonoBehaviour
{
    string[] m_config;
    [SerializeField] UDPSend m_udpSend;
    [SerializeField] UDPReceive m_udpReceiver;
    [SerializeField] int m_sendingPort;
    [SerializeField] int m_receivingPort;
    [SerializeField] string m_ipAddress;
    [SerializeField] string m_appID;
    [SerializeField] string m_receiveID;
    [SerializeField] GameObject m_loadingPanel;
    [SerializeField] string m_dataUrl;
    //float[] m_delayTime;
    int m_state = 0;

    void Start()
    {
        //init config
        m_config = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/"+ m_dataUrl + "/" + m_appID + "Config.ini");
        m_ipAddress = m_config[0].Split('=')[1];
        m_sendingPort = int.Parse(m_config[1].Split('=')[1]);
        m_receivingPort = int.Parse(m_config[2].Split('=')[1]);
        //m_delayTime = new float[3];
        //m_delayTime[0] = float.Parse(m_config[3].Split('=')[1]);
        //m_delayTime[1] = float.Parse(m_config[4].Split('=')[1]);
        //m_delayTime[2] = float.Parse(m_config[5].Split('=')[1]);

        m_udpSend.SetPort(m_sendingPort);
        m_udpReceiver.SetPort(m_receivingPort);
        m_udpReceiver.receiveUDP += OnReceive;

        for (int i = 0; i < Display.displays.Length; i++)
            Display.displays[i].Activate();
    }

    void OnReceive(string p_data)
    {
        string[] data = p_data.Split('|');

        if (data[0] != m_appID)
            return;

        if (data[1] == "DONE")
            m_loadingPanel.SetActive(false);


    }

    public void SendCommand(int p_state) {
        if (m_state == p_state)
            return;
      
        string message = m_appID + "|POS" + p_state;
        m_udpSend.SendUDPMsg(message, m_ipAddress);
        m_loadingPanel.SetActive(true);
        //StartCoroutine(ShowLoadingScreen(m_delayTime[p_state - 1]));
        m_state = p_state;
    }

    IEnumerator ShowLoadingScreen(float p_delayTime)
    {
        m_loadingPanel.SetActive(true);
        yield return new WaitForSeconds(p_delayTime);
        m_loadingPanel.SetActive(false);
    }

    public void SendToLaptop(string p_msge)
    {
        string message = m_receiveID + "|" + p_msge;
        m_udpSend.SendUDPMsg(message, m_ipAddress);
    }
}
