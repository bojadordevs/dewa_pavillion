using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using RenderHeads.Media.AVProVideo;
using RenderHeads.Media.AVProLiveCamera;

public class AppManagerRoboticArm : MonoBehaviour
{
    string[] m_config;
    [SerializeField] UDPSend m_udpSend;
    [SerializeField] UDPReceive m_udpReceiver;
    [SerializeField] UDPSend m_udpSendRA;
    [SerializeField] string m_ipAddress;
    [SerializeField] int m_sendingPort;
    [SerializeField] int m_receivingPort;

    [SerializeField] string m_ipAddressRA;
    [SerializeField] int m_sendingPortRA;

    [SerializeField] string m_appID;
    [SerializeField] string m_receiverID;

    [SerializeField] MediaPlayer m_mediaPlayer;
    [SerializeField] AVProLiveCamera m_liveCamera;

    [SerializeField] string m_dataUrl;

    float m_videoPlayDelay = 0;
    string roboticArmID = "RA";


    void Start()
    {
        //init config
        m_config = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/" + m_dataUrl + "/" + m_appID + "Config.ini");
        m_ipAddress = m_config[0].Split('=')[1];
        m_sendingPort = int.Parse(m_config[1].Split('=')[1]);
        m_receivingPort = int.Parse(m_config[2].Split('=')[1]);

        m_ipAddressRA = m_config[3].Split('=')[1];
        m_sendingPortRA = int.Parse(m_config[4].Split('=')[1]);
        m_videoPlayDelay = float.Parse(m_config[5].Split('=')[1]);

        m_udpSend.SetPort(m_sendingPort);
        m_udpReceiver.SetPort(m_receivingPort);
        m_udpSendRA.SetPort(m_sendingPortRA);
        m_udpReceiver.receiveUDP += OnReceive;
        StartCoroutine(PlayVideo("ScreenSaver.mp4", true));

        for (int i = 0; i < Display.displays.Length; i++)
            Display.displays[i].Activate();
    }

    void OnReceive(string p_data)
    {
        string[] data = p_data.Split('|');

        if (data[0] != m_appID)
            return;

        switch (data[1])
        {
            case "POS1":
                GoToPosition("POS1");
                break;
            case "POS2":
                GoToPosition("POS2");
                break;
            case "POS3":
                GoToPosition("POS3");
                break;
            //Play video after reaching the position
            case "PLAY1":
                StartCoroutine( PlayVideo("V1.mp4"));
                break;
            case "PLAY2":
                StartCoroutine(PlayVideo("V2.mp4"));
                break;
            case "PLAY3":
                StartCoroutine(PlayVideo("V3.mp4"));
                break;
            case "SCREEN":
                StartCoroutine(PlayVideo("ScreenSaver.mp4",true));
                SendToTablet("DONE");
                break;
        }
    }

    private void Update()
    {
        if (m_mediaPlayer != null)
        {
            if (m_mediaPlayer.Control.IsPlaying() && m_mediaPlayer.Control.IsFinished())
            {
                m_mediaPlayer.Stop();
                Invoke("GoToHome", m_videoPlayDelay);
            }

        }

        //Debug
        if (Input.GetKeyDown(KeyCode.Alpha1))
            OnReceive("APP2|POS1");
        if (Input.GetKeyDown(KeyCode.Alpha2))
            OnReceive("APP2|POS2");
        if (Input.GetKeyDown(KeyCode.Alpha3))
            OnReceive("APP2|POS3");

        if (Input.GetKeyDown(KeyCode.Alpha4))
            OnReceive("APP2|PLAY1");
        if (Input.GetKeyDown(KeyCode.Alpha5))
            OnReceive("APP2|PLAY2");
        if (Input.GetKeyDown(KeyCode.Alpha6))
            OnReceive("APP2|PLAY3");

        if (Input.GetKeyDown(KeyCode.S))
            OnReceive("APP2|SCREEN");

    }

    void GoToHome()
    {
        m_mediaPlayer.CloseVideo();
        SendToRoboticArm("HOME"); //wait for the SCREEN command from Robotic ARM
    }

    void GoToPosition(string p_msge)
    {
        m_mediaPlayer.CloseVideo();
        SendToRoboticArm(p_msge); // relay message to robotic arm
    }

    IEnumerator PlayVideo(string p_video,bool p_loop = false)
    {
        yield return new WaitForSeconds(p_loop ? 0 : m_videoPlayDelay);
        m_mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, m_dataUrl + "/" + p_video, true);
        m_mediaPlayer.Control.SetLooping(p_loop);
    }

    public void SendToTablet(string p_msge)
    {
        string message = m_receiverID + "|" + p_msge;
        m_udpSend.SendUDPMsg(message, m_ipAddress);
        Debug.Log("SendToTablet: " + message);
    }

    public void SendToRoboticArm(string p_msge)
    {
        string message = roboticArmID + "|" + p_msge;
        m_udpSendRA.SendUDPMsg(message, m_ipAddress);
        Debug.Log("SendToRoboticArm: " + message);
    }
}
