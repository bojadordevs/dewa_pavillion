using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using RenderHeads.Media.AVProVideo;

public class AppManagerApp4 : MonoBehaviour
{
    string[] m_config;
    [SerializeField] UDPReceive m_udpReceiver;
    [SerializeField] string m_ipAddress;
    [SerializeField] int m_receivingPort;
    [SerializeField] string m_appID;
    [SerializeField] string m_receiverID;
    [SerializeField] MediaPlayer m_mediaPlayer1;
    [SerializeField] MediaPlayer m_mediaPlayer2;
    [SerializeField] string m_dataUrl;

    string m_screensaverVideo1;
    string m_selectionVideo1;
    string m_screensaverVideo2;
    string m_selectionVideo2;

    float m_videoPlayDelay = 0;


    void Start()
    {
        //init config
        m_config = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/" + m_dataUrl + "/" + m_appID + "Config.ini");
        m_ipAddress = m_config[0].Split('=')[1];
        m_receivingPort = int.Parse(m_config[1].Split('=')[1]);

        m_screensaverVideo1 = m_config[2].Split('=')[1];
        m_selectionVideo1 = m_config[3].Split('=')[1];

        m_screensaverVideo2 = m_config[4].Split('=')[1];
        m_selectionVideo2 = m_config[5].Split('=')[1];

        m_udpReceiver.SetPort(m_receivingPort);
        m_udpReceiver.receiveUDP += OnReceive;
        PlayVideo1(m_screensaverVideo1, true);
        PlayVideo2(m_screensaverVideo2, true);

        for (int i = 0; i < Display.displays.Length; i++)
            Display.displays[i].Activate();
    }

    void OnReceive(string p_data)
    {
        string[] data = p_data.Split('|');

        if (data[0] != m_appID)
            return;

        switch (data[1])
        {
            case "START":
                PlayVideo1(m_selectionVideo1);
                PlayVideo2(m_selectionVideo2);
                break;
            case "DONE":
                PlayVideo1(m_screensaverVideo1, true);
                PlayVideo2(m_screensaverVideo2, true);
                break;
        }
    }

    private void Update()
    {
        if (m_mediaPlayer1 != null) //Only media player 1 is the reference
        {
            if (m_mediaPlayer1.Control.IsPlaying() && m_mediaPlayer1.Control.IsFinished())
            {
                m_mediaPlayer1.Stop();
                PlayVideo1(m_screensaverVideo1, true);
                PlayVideo2(m_screensaverVideo2, true);
            }
        }

        //Debug
        if (Input.GetKeyDown(KeyCode.Alpha1))
            OnReceive(m_appID + "|START");
        if (Input.GetKeyDown(KeyCode.S))
            OnReceive(m_appID + "|DONE");
    }

    void PlayVideo1(string p_video, bool p_loop = false)
    {
        m_mediaPlayer1.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, m_dataUrl + "/" + p_video, true);
        m_mediaPlayer1.Control.SetLooping(p_loop);
    }

    void PlayVideo2(string p_video, bool p_loop = false)
    {
        m_mediaPlayer2.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, m_dataUrl + "/" + p_video, true);
        m_mediaPlayer2.Control.SetLooping(p_loop);
    }
}
