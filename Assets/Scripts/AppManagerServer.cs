using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;

public class AppManagerServer : MonoBehaviour
{
    [SerializeField] MediaPlayer m_mediaPlayer;
    [SerializeField] UDPReceive m_udpReceiver;
    [SerializeField] int m_port;
    [SerializeField] string m_ipAddress;
    [SerializeField] string m_appID;

    void Awake()
    {
        //m_udpReceiver.SetPort(m_port);
        m_udpReceiver.receiveUDP += OnReceive;
        Debug.Log(this.name);
    }


    protected virtual void OnReceive(string p_data)
    {
        string[] data = p_data.Split('|');
        //       string message = m_appID + "|cmd1|" + p_val;

        if (data[0] != m_appID)
            return;

        switch (data[1])
        {
            case "cmd1":
                PlayMediaPlayer();
                break;
            case "cmd2":
                PauseMediaPlayer();
                break;
            case "cmd3":
                StopMediaPlayer();
                break;
        }
    }

    public void PlayMediaPlayer()
    {
        if(!m_mediaPlayer.Control.IsPlaying())
            m_mediaPlayer.Control.Play();
    }

    public void PauseMediaPlayer()
    {
        if (m_mediaPlayer.Control.IsPlaying())
            m_mediaPlayer.Control.Pause();
    }

    public void StopMediaPlayer()
    {
        m_mediaPlayer.Control.Rewind();
        m_mediaPlayer.Control.Stop();
    }
}
