using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManagerTablet6: MonoBehaviour
{
    string[] m_config;

    [SerializeField] UDPSend m_udpSendWatchout;
    [SerializeField] int m_sendingPortWatchout;
    [SerializeField] string m_ipAddressWatchout;
    [SerializeField] string m_WatchoutCmd1;
    [SerializeField] string m_WatchoutCmd2;

    [SerializeField] string m_appID;
    [SerializeField] GameObject m_loadingPanel;
    [SerializeField] string m_dataUrl;

    float m_loadingDelay = 0;

    void Start()
    {
        //init config
        m_config = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/" + m_dataUrl + "/" + m_appID + "Config.ini");
        m_ipAddressWatchout = m_config[0].Split('=')[1];
        m_sendingPortWatchout = int.Parse(m_config[1].Split('=')[1]);
        m_udpSendWatchout.SetPort(m_sendingPortWatchout);

        m_WatchoutCmd1 = m_config[2].Split('=')[1];
        m_WatchoutCmd2 = m_config[3].Split('=')[1];
        m_loadingDelay = float.Parse(m_config[4].Split('=')[1]);

        for (int i = 0; i < Display.displays.Length; i++)
            Display.displays[i].Activate();
    }

    public void SendCommand(int p_command)
    {
        if (p_command == 1)
        {
            SendToWatchout("kill " + m_WatchoutCmd2);
            SendToWatchout("run " + m_WatchoutCmd1);
        }
        else if (p_command == 2)
        {
            SendToWatchout("kill " + m_WatchoutCmd1);
            SendToWatchout("run " + m_WatchoutCmd2);
        }
        //StartCoroutine(ShowLoadingScreen(m_loadingDelay));
    }

    IEnumerator ShowLoadingScreen(float p_delayTime)
    {
        m_loadingPanel.SetActive(true);
        yield return new WaitForSeconds(p_delayTime);
        m_loadingPanel.SetActive(false);
    }

    public void SendToWatchout(string p_message)
    {
        m_udpSendWatchout.SendUDPMsg(p_message + "\n", m_ipAddressWatchout);
    }
}
