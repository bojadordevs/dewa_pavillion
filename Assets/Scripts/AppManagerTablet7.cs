using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppManagerTablet7 : MonoBehaviour
{
    string[] m_config;
    [SerializeField] UDPSend m_udpSendToLaptop;
    [SerializeField] UDPReceive m_udpReceiveFromLaptop;
    [SerializeField] UDPSend m_udpSendToControllino;
    [SerializeField] int m_sendingPort;
    [SerializeField] int m_receivingPort;
    [SerializeField] string m_ipAddress;

    [SerializeField] int m_sendingPortControllino;
    [SerializeField] string m_ipAddressControllino;

    [SerializeField] string m_appID;
    [SerializeField] string m_receiveID;
    [SerializeField] GameObject m_loadingPanel;
    [SerializeField] string m_dataUrl;
    private void Awake()
    {
        for (int i = 0; i < Display.displays.Length; i++)
            Display.displays[i].Activate();
    }

    void Start()
    {
        //init config
        m_config = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/" + m_dataUrl + "/" + m_appID + "Config.ini");
        m_ipAddress = m_config[0].Split('=')[1];
        m_sendingPort = int.Parse(m_config[1].Split('=')[1]);
        m_receivingPort = int.Parse(m_config[2].Split('=')[1]);

        m_ipAddressControllino = m_config[3].Split('=')[1];
        m_sendingPortControllino = int.Parse(m_config[4].Split('=')[1]);

        m_udpSendToLaptop.SetPort(m_sendingPort);
        m_udpReceiveFromLaptop.SetPort(m_receivingPort);
        m_udpSendToControllino.SetPort(m_sendingPortControllino);
        m_udpReceiveFromLaptop.receiveUDP += OnReceive;


    }

    void OnReceive(string p_data)
    {
        string[] data = p_data.Split('|');

        if (data[0] != m_appID)
            return;

        if (data[1] == "DONE")
        {
            m_loadingPanel.SetActive(false);
            SceneManager.LoadScene("App10-[Tablet]");
        }

    }

    public void SendCommand()
    {
        SendToLaptop("START");
        m_udpSendToControllino.SendUDPMsg("LED", m_ipAddressControllino);
        m_loadingPanel.SetActive(true);
        //StartCoroutine(ShowLoadingScreen(1));
    }

    IEnumerator ShowLoadingScreen(float p_delayTime)
    {
        m_loadingPanel.SetActive(true);
        yield return new WaitForSeconds(p_delayTime);
        m_loadingPanel.SetActive(false);
    }

    public void SendToLaptop(string p_msge)
    {
        string message = m_receiveID + "|" + p_msge;
        m_udpSendToLaptop.SendUDPMsg(message, m_ipAddress);
    }
}
