using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManagerClient : MonoBehaviour
{
    const string CONFIG = "ConfigClient.ini";
    string[] m_config;
    [SerializeField] UDPSend m_udpSend;
    [SerializeField] int m_port;
    [SerializeField] string m_ipAddress;
    [SerializeField] string m_appID;
    [SerializeField] bool m_useID = false;

    void Start()
    {
        //init config
        m_config = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/Config/" + CONFIG);
        m_port = int.Parse(m_config[0].Split('=')[1]);
        m_ipAddress = m_config[1].Split('=')[1];
        if(m_useID)
            m_appID = m_config[2].Split('=')[1];
    }

    public void SendCommand1(bool p_val)
    {
        string message = m_appID + "|cmd1|" + p_val;
        m_udpSend.SendUDPMsg(message, m_ipAddress);
    }

    public void SendCommand2(bool p_val)
    {
        string message = m_appID + "|cmd2|" + p_val;
        m_udpSend.SendUDPMsg(message, m_ipAddress);
    }

    public void SendCommand3(bool p_val)
    {
        string message = m_appID + "|cmd3|" + p_val;
        m_udpSend.SendUDPMsg(message, m_ipAddress);
    }

    public void SendToServer(string p_msge)
    {
        string message = m_appID + "|" + p_msge;
        m_udpSend.SendUDPMsg(message, m_ipAddress);
    }
}
