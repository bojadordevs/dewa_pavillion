using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using RenderHeads.Media.AVProVideo;

public class AppManagerTablet3 : MonoBehaviour
{
    string[] m_config;
    [SerializeField] UDPSend m_udpSend;
    [SerializeField] int m_sendingPort;
    [SerializeField] string m_ipAddress;
    [SerializeField] string m_appID;
    [SerializeField] string m_receiverID;
    [SerializeField] GameObject m_loadingPanel;
    [SerializeField] string m_dataUrl;
    [SerializeField] MediaPlayer m_mediaPlayer;
    float m_delayTime;

    void Start()
    {
        //init config
        m_config = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/" + m_dataUrl + "/" + m_appID + "Config.ini");
        m_ipAddress = m_config[0].Split('=')[1];
        m_sendingPort = int.Parse(m_config[1].Split('=')[1]);
        m_delayTime = float.Parse(m_config[2].Split('=')[1]);
        m_udpSend.SetPort(m_sendingPort);
        //PlayVideo();

        for (int i = 0; i < Display.displays.Length; i++)
            Display.displays[i].Activate();
    }

    public void SendCommand()
    {
        StopAllCoroutines();
        //if (p_val)
        //{
        //    SetChannel("DMXConfigOn.ini");
        //    StartCoroutine(SendPlayVideo(p_val));
        //}
        //else
        //{
        //    SetChannel("DMXConfigOff.ini");
        //    StartCoroutine(SendPlayVideo(p_val));
        //}
        SetChannel("DMXConfig.ini");
        StartCoroutine(SendPlayVideo(true));
        StartCoroutine(ShowLoadingScreen(1));
    }

    IEnumerator SendPlayVideo(bool p_val)
    {
        yield return new WaitForSeconds(m_delayTime);
        SendToLaptop("START");
    }

    public void SetChannel(string p_commandName)
    {
        string[] dmxConfig = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/" + m_dataUrl + "/" + p_commandName);
        int universe = 0;
        List<Channel> channelList = new List<Channel>();
        for (int i = 0; i < dmxConfig.Length; i++)
        {
            if (i == 0)
            {
                universe = int.Parse(dmxConfig[i].Split('=')[1]);
            }
            else
            {
                Channel channel = new Channel();
                channel.index = int.Parse(dmxConfig[i].Split(',')[0].Split('=')[1]);
                channel.val = System.Convert.ToByte(dmxConfig[i].Split(',')[1].Split('=')[1]);
                channelList.Add(channel);
            }
        }
        ArtnetController.instance.SetChannelByUniverse(universe, channelList);
    }

    void PlayVideo(string p_video, bool p_loop = false)
    {
        m_mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, m_dataUrl + "/" + p_video, true);
        m_mediaPlayer.Control.SetLooping(p_loop);
    }

    IEnumerator ShowLoadingScreen(float p_delayTime)
    {
        m_loadingPanel.SetActive(true);
        yield return new WaitForSeconds(p_delayTime);
        m_loadingPanel.SetActive(false);
    }

    public void SendToLaptop(string p_msge)
    {
        string message = m_receiverID + "|" + p_msge;
        m_udpSend.SendUDPMsg(message, m_ipAddress);
        Debug.Log("SendToLaptop: " + message);
    }
}
