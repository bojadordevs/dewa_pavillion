using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManagerTablet9 : MonoBehaviour
{
    string[] m_config;

    [SerializeField] UDPSend m_udpSendWatchout;
    [SerializeField] int m_sendingPortWatchout;
    [SerializeField] string m_ipAddressWatchout;
    [SerializeField] string m_WatchoutCmd;
    [SerializeField] string m_appID;
    [SerializeField] GameObject m_loadingPanel;
    [SerializeField] string m_dataUrl;

    float m_loadingDelay = 0;


    void Start()
    {
        m_config = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/" + m_dataUrl + "/" + m_appID + "Config.ini");

        m_ipAddressWatchout = m_config[0].Split('=')[1];
        m_sendingPortWatchout = int.Parse(m_config[1].Split('=')[1]);
        m_udpSendWatchout.SetPort(m_sendingPortWatchout);

        m_WatchoutCmd = m_config[2].Split('=')[1];
        m_loadingDelay = float.Parse(m_config[3].Split('=')[1]);

        for (int i = 0; i < Display.displays.Length; i++)
            Display.displays[i].Activate();
    }

    public void SendCommand(bool p_val)
    {
        StartCoroutine(ShowLoadingScreen(m_loadingDelay));
        SendToWatchout("run " + m_WatchoutCmd);
    }

    public void SendToWatchout(string p_message)
    {
        m_udpSendWatchout.SendUDPMsg(p_message + "\n", m_ipAddressWatchout);
    }

    IEnumerator ShowLoadingScreen(float p_delayTime)
    {
        m_loadingPanel.SetActive(true);
        yield return new WaitForSeconds(p_delayTime);
        m_loadingPanel.SetActive(false);
    }
}
