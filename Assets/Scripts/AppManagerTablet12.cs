using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppManagerTablet12 : MonoBehaviour
{
    string[] m_config;
    [SerializeField] UDPSend m_udpSendToWatchout;
    [SerializeField] UDPReceive m_udpReceiveFromControllino;
    [SerializeField] UDPSend m_udpSendToControllino;
    [SerializeField] int m_sendingPortWatchout;
    [SerializeField] int m_receivingPortControllino;
    [SerializeField] string m_ipAddressWatchout;

    [SerializeField] int m_sendingPortControllino;
    [SerializeField] string m_ipAddressControllino;

    [SerializeField] string m_appID;
    [SerializeField] GameObject m_loadingPanel;
    [SerializeField] string m_dataUrl;

    string m_WatchoutTimeline1;
    string m_WatchoutTimeline2;
    string m_WatchoutTimeline3;
    string m_WatchoutTimeline4;

    bool m_isControllinoOpen = false;

    void Start()
    {
        //init config
        m_config = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/" + m_dataUrl + "/" + m_appID + "Config.ini");
        m_ipAddressWatchout = m_config[0].Split('=')[1];
        m_sendingPortWatchout = int.Parse(m_config[1].Split('=')[1]);

        m_ipAddressControllino = m_config[2].Split('=')[1];
        m_sendingPortControllino = int.Parse(m_config[3].Split('=')[1]);
        m_receivingPortControllino = int.Parse(m_config[4].Split('=')[1]);

        m_WatchoutTimeline1 = m_config[5].Split('=')[1];
        m_WatchoutTimeline2 = m_config[6].Split('=')[1];
        m_WatchoutTimeline3 = m_config[7].Split('=')[1];
        m_WatchoutTimeline4 = m_config[8].Split('=')[1];

        //WO
        m_udpSendToWatchout.SetPort(m_sendingPortWatchout);
        //Controllino
        m_udpReceiveFromControllino.SetPort(m_receivingPortControllino);
        m_udpSendToControllino.SetPort(m_sendingPortControllino);
        m_udpReceiveFromControllino.receiveUDP += OnReceive;

        for (int i = 0; i < Display.displays.Length; i++)
            Display.displays[i].Activate();
    }

    void OnReceive(string p_data)
    {
        string[] data = p_data.Split('|');

        if (data[0] != m_appID)
            return;

        if (data[1] == "DONE")
        {
            m_loadingPanel.SetActive(false);
            //SceneManager.LoadScene("App12-[Tablet]");
        }

    }

    public void SendCommand()
    {
        SendToWatchout("OPEN");
        m_udpSendToControllino.SendUDPMsg("OPEN", m_ipAddressControllino);
        m_loadingPanel.SetActive(true);
        //StartCoroutine(ShowLoadingScreen(1));
    }

    void WOKillAll()
    {
        SendToWatchout("kill " + m_WatchoutTimeline1);
        SendToWatchout("kill " + m_WatchoutTimeline2);
        SendToWatchout("kill " + m_WatchoutTimeline3);
        SendToWatchout("kill " + m_WatchoutTimeline4);
    }

    public void SendCommand(int p_command)
    {
        WOKillAll();
        switch (p_command) {
            case 1:
                SendToWatchout("run " + m_WatchoutTimeline1);
                break;
            case 2:
                SendToWatchout("run " + m_WatchoutTimeline2);
                break;
            case 3:
                SendToWatchout("run " + m_WatchoutTimeline3);
                break;
            case 4:
                SendToWatchout("run " + m_WatchoutTimeline4);
                break;
        }
        m_isControllinoOpen = !m_isControllinoOpen;
        m_udpSendToControllino.SendUDPMsg(m_isControllinoOpen ? "OPEN" : "CLOSE" , m_ipAddressControllino);
        m_loadingPanel.SetActive(true);
    }

    //IEnumerator ShowLoadingScreen(float p_delayTime)
    //{
    //    m_loadingPanel.SetActive(true);
    //    yield return new WaitForSeconds(p_delayTime);
    //    m_loadingPanel.SetActive(false);
    //}

    public void SendToWatchout(string p_message)
    {
        m_udpSendToWatchout.SendUDPMsg(p_message + "\n", m_ipAddressWatchout);
    }
}
