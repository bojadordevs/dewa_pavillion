using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManagerTablet5 : MonoBehaviour
{
    string[] m_config;
    [SerializeField] UDPSend m_udpSendControllino;
    [SerializeField] int m_sendingPortControllino;
    [SerializeField] string m_ipAddressControllino;
    [SerializeField] string m_controllinoCmdUp;
    [SerializeField] string m_controllinoCmdDown;

    [SerializeField] UDPSend m_udpSendWatchout;
    [SerializeField] int m_sendingPortWatchout;
    [SerializeField] string m_ipAddressWatchout;
    [SerializeField] string m_WatchoutCmdUp;
    [SerializeField] string m_WatchoutCmdDown;


    [SerializeField] string m_appID;
    [SerializeField] GameObject m_loadingPanel;
    [SerializeField] string m_dataUrl;

    float m_loadingDelay = 0;



    void Start()
    {
        //init config
        m_config = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/" + m_dataUrl + "/" + m_appID + "Config.ini");
        m_ipAddressControllino = m_config[0].Split('=')[1];
        m_sendingPortControllino = int.Parse(m_config[1].Split('=')[1]);
        m_udpSendControllino.SetPort(m_sendingPortControllino);

        m_ipAddressWatchout = m_config[2].Split('=')[1];
        m_sendingPortWatchout = int.Parse(m_config[3].Split('=')[1]);
        m_udpSendWatchout.SetPort(m_sendingPortWatchout);

        m_controllinoCmdUp = m_config[4].Split('=')[1];
        m_WatchoutCmdUp = m_config[5].Split('=')[1];
        m_controllinoCmdDown = m_config[6].Split('=')[1];
        m_WatchoutCmdDown = m_config[7].Split('=')[1];
        m_loadingDelay = float.Parse(m_config[8].Split('=')[1]);

        for (int i = 0; i < Display.displays.Length; i++)
            Display.displays[i].Activate();
    }

    public void SendCommand(bool p_val)
    {
        StartCoroutine(ShowLoadingScreen(m_loadingDelay));
        if (p_val)
        {
            m_udpSendControllino.SendUDPMsg(m_controllinoCmdUp, m_ipAddressControllino);
            //SendToWatchout("kill " + m_WatchoutCmdDown);
            SendToWatchout("run " + m_WatchoutCmdUp);
        }
        else
        {
            m_udpSendControllino.SendUDPMsg(m_controllinoCmdDown, m_ipAddressControllino);
            //SendToWatchout("kill " + m_WatchoutCmdUp);
            //SendToWatchout("run " + m_WatchoutCmdDown);
        }

    }

    public void SendToWatchout(string p_message)
    {
        m_udpSendWatchout.SendUDPMsg(p_message + "\n", m_ipAddressWatchout);
    }

    IEnumerator ShowLoadingScreen(float p_delayTime)
    {
        m_loadingPanel.SetActive(true);
        yield return new WaitForSeconds(p_delayTime);
        m_loadingPanel.SetActive(false);
    }
}
