﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;

public class AppManagerSlidingScreen : MonoBehaviour
{
    static float MAX_SCREEN_POSITION = 1280;
    string[] m_config;
    [SerializeField] UDPSend m_udpSend;
    [SerializeField] UDPReceive m_udpReceiver;
    [SerializeField] UDPSend m_udpSendMotor;
    [SerializeField] UDPReceive m_udpReceiverEncoder;
    [SerializeField] string m_ipAddress;
    [SerializeField] int m_sendingPort;
    [SerializeField] int m_receivingPort;
    [SerializeField] string m_ipAddressMotor;
    [SerializeField] int m_sendingPortMotor;
    [SerializeField] int m_receiverPortMotor;
    [SerializeField] string m_appID;
    [SerializeField] string m_receiverID;
    [SerializeField] GameObject m_SlidingScreenObj;
    [SerializeField] MediaPlayer m_mediaPlayerSlidingScreen;
    [SerializeField] string m_dataUrl;

    [SerializeField] float m_videoPlayDelay = 0;
    [SerializeField] float m_stopPos1 = 0;
    [SerializeField] float m_stopPos2 = 0;
    [SerializeField] float m_maxEncVal = 120;
    float m_encoderVal = 0;
    SlidingState m_currentState;
    float m_currentTargetPos = 0;
    bool m_isVideoPlaying = false;

    enum SlidingState
    {
        HOME,
        MOVING_TO_POS,
        ON_POS,
    }

    //Debug
    float TempEncoderVal = 0;

    void Start()
    {
        //init config
        m_config = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/" + m_dataUrl + "/" + m_appID + "Config.ini");
        m_ipAddress = m_config[0].Split('=')[1];
        m_sendingPort = int.Parse(m_config[1].Split('=')[1]);
        m_receivingPort = int.Parse(m_config[2].Split('=')[1]);

        m_ipAddressMotor = m_config[3].Split('=')[1];
        m_sendingPortMotor = int.Parse(m_config[4].Split('=')[1]);
        m_receiverPortMotor = int.Parse(m_config[5].Split('=')[1]);
        m_videoPlayDelay = float.Parse(m_config[6].Split('=')[1]);

        m_stopPos1 = float.Parse(m_config[7].Split('=')[1]);
        m_stopPos2 = float.Parse(m_config[8].Split('=')[1]);
        m_maxEncVal = float.Parse(m_config[9].Split('=')[1]);
        //Set Ports
        m_udpSend.SetPort(m_sendingPort);
        m_udpReceiver.SetPort(m_receivingPort);
        m_udpSendMotor.SetPort(m_sendingPortMotor);
        m_udpReceiverEncoder.SetPort(m_receiverPortMotor);
        //Set Receivers
        m_udpReceiver.receiveUDP += OnReceiveFromTablet;
        m_udpReceiverEncoder.receiveUDP += OnReceiveEncoder;
        m_currentState = SlidingState.HOME;
        m_isVideoPlaying = false;
        m_mediaPlayerSlidingScreen.GetComponent<Animator>().SetBool("VideoShowing", m_isVideoPlaying);

        for (int i = 0; i < Display.displays.Length; i++)
            Display.displays[i].Activate();
    }

    void OnReceiveFromTablet(string p_data)
    {
        string[] data = p_data.Split('|');

        if (data[0] != m_appID)
            return;

        switch (data[1])
        {
            case "POS1":
                m_currentTargetPos = m_stopPos1;
                GoToPosition("POS1");
                break;
            case "POS2":
                m_currentTargetPos = m_stopPos2;
                GoToPosition("POS2");
                break;
            case "HOME":
                m_currentTargetPos = 0;
                GoToPosition("HOME");
                break;
        }
    }

    void OnReceiveEncoder(string p_data)
    {
        //Debug.Log("OnReceiveEncoder: " + p_data);
        string[] data = p_data.Split('|');

        if (data[0] != m_appID)
            return;

        float encoderVal = float.Parse(data[1]);

        if(m_encoderVal != encoderVal)
        {
            m_encoderVal = encoderVal;
            if (encoderVal <= 0 && encoderVal >= -m_maxEncVal)
            {
                float convertedVal = (encoderVal / m_maxEncVal) * MAX_SCREEN_POSITION;
                m_SlidingScreenObj.GetComponent<RectTransform>().anchoredPosition = new Vector2(convertedVal, 0);
                if(-m_encoderVal > (m_currentTargetPos - 1) && -m_encoderVal < (m_currentTargetPos + 1) && m_currentState == SlidingState.MOVING_TO_POS)
                {
                    StopAllCoroutines();
                    SendToTablet("DONE");
                    StartCoroutine(PlayVideo("V1.mp4"));
                    m_currentState = SlidingState.ON_POS;
                }
            }
        }
    }

    private void Update()
    {
        if (m_mediaPlayerSlidingScreen != null && m_mediaPlayerSlidingScreen.Info.GetDurationMs() > 0)
        {
            if (m_isVideoPlaying && m_mediaPlayerSlidingScreen.Control.GetCurrentTimeMs() > m_mediaPlayerSlidingScreen.Info.GetDurationMs() - 3)
            {
                Debug.Log("CLOSE VID: " + m_mediaPlayerSlidingScreen.Info.GetDurationMs());
                m_isVideoPlaying = false;
                StartCoroutine(CloseVideo());
            }
        }

        Debug.Log("POSITION: " + m_encoderVal + " : " + m_currentTargetPos  + " : " + m_currentState);

        //Debug
        if (Input.GetKeyDown(KeyCode.Alpha1))
            OnReceiveFromTablet("APP11|POS1");
        if (Input.GetKeyDown(KeyCode.Alpha2))
            OnReceiveFromTablet("APP11|POS2");

        if (Input.GetKey(KeyCode.LeftArrow) && m_currentState != SlidingState.ON_POS)
            OnReceiveEncoder("APP11|" + (TempEncoderVal += 1));
        if (Input.GetKey(KeyCode.RightArrow) && m_currentState != SlidingState.ON_POS)
            OnReceiveEncoder("APP11|" + (TempEncoderVal -= 1));

    }

    void GoToHome()
    {
        m_mediaPlayerSlidingScreen.CloseVideo();
        SendToMotor("HOME"); //wait for the SCREEN command from Robotic ARM
        m_currentState = SlidingState.HOME;
        m_currentTargetPos = 0;
    }

    void GoToPosition(string p_msge)
    {
        m_mediaPlayerSlidingScreen.CloseVideo();
        m_currentState = SlidingState.MOVING_TO_POS;
        SendToMotor(p_msge); // relay message to motor
    }

    IEnumerator PlayVideo(string p_video, bool p_loop = false)
    {
        yield return new WaitForSeconds(p_loop ? 0 : m_videoPlayDelay);
        m_mediaPlayerSlidingScreen.GetComponent<Animator>().SetBool("VideoShowing",true);
        m_mediaPlayerSlidingScreen.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, m_dataUrl + "/" + p_video, true);
        m_mediaPlayerSlidingScreen.Control.SetLooping(p_loop);
        m_mediaPlayerSlidingScreen.Play();
        m_isVideoPlaying = true;
    }

    IEnumerator CloseVideo()
    {
        m_mediaPlayerSlidingScreen.GetComponent<Animator>().SetBool("VideoShowing", false);
        yield return new WaitForSeconds(1);
        m_mediaPlayerSlidingScreen.Stop();
        yield return new WaitForSeconds(m_videoPlayDelay);
        GoToHome();
    }

    public void SendToTablet(string p_msge)
    {
        string message = m_receiverID + "|" + p_msge;
        m_udpSend.SendUDPMsg(message, m_ipAddress);
    }

    public void SendToMotor(string p_msge)
    {
        string message =  "SS|" + p_msge;
        m_udpSendMotor.SendUDPMsg(message, m_ipAddressMotor);
    }
}
